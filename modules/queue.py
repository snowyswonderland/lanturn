import discord
from discord.ext import commands, tasks
from framecalc import *
from seedgen import *
from GetPokeInfo import *

class RaidCommands(commands.Cog):
  def __init__(self, bot):
    self.checkDataReady.start()
    self.bot = bot  
    self.person = None
    self.userChannel = None

  @commands.command(name='checkmyseed')
  async def checkMySeed(self, ctx):
    async with self.bot.db.acquire() as db:
      queue = await db.fetch("SELECT * FROM queue ORDER BY place ASC NULLS LAST")
      length = len(queue)
      if ctx.message.guild != None:
        id = ctx.author.id
        person_found = False
        oipif = None
        thonk=dict(queue[length-1])
        for person in queue:
          person_dict = dict(person)
          if person_dict['user_id'] == id:
            person_found = True
            oipif = person_dict['place']
        if person_found == True:
          await ctx.send(f"You're already in the queue! Your current position is {oipif}!")
        else:
          if self.person == None:
            await db.execute(f"INSERT INTO queue(user_id, place) VALUES({id}, {thonk['place'] + 1})")
            await ctx.send("You have been added to the queue! There is no one ahead of you, so I will ping you when I start searching!")
          else:
            prsn = ""
            pre = ""
            if length == 1:
              prsn = " person "
              pre = " is "
            else:
              prsn = " people "
              pre = " are "
            await ctx.send(f"You have been added to the queue! There {pre} {length} {prsn} ahead of you! I will ping you when it is your turn!")
      await self.bot.db.release(db)

  @tasks.loop(seconds=.01)
  async def checkDataReady(self):
    queue = await self.bot.db.fetch("SELECT * FROM queue ORDER BY place ASC NULLS LAST")
    length = len(queue)
    queue2 = []
    for i in queue:
      queue2.append(dict(i))
    if length == 0 and self.person == None:
      initializeDuduClient()
    if checkSearchStatus() == 1 and self.person != None:
      self.userChannel = discord.utils.get(self.bot.get_all_channels(), guild_name='BLAINES', name='subbie-cmds')
      self.person = discord.utils.get(self.bot.get_all_members(), id=queue2[0]['user_id'])
      code = getCodeString()
      await self.userChannel.send(f"<@{queue2[0]['user_id']}>, I am now searching! I have DMed you your unique link code. My in game name is Lanturn.")
      await self.person.send(f"```py\nHi there! Your private link code is {code}.\nPlease use it to match with me in trade!```")
      
    if checkTimeOut() and self.userChannel != None:
      self.userChannel.send(f"<@{self.person.id}>, the trade has timed out! Either you took too long to respond or you lost connection. You have been dequeued.")
      await self.bot.db.execute(f"DELETE FROM queue WHERE user_id={self.person.id}")
      self.userChannel = None
      self.person = None

    if checkDuduStatus() == False and self.userChannel != None:
      time.sleep(2.0)
      ec, pid, seed, ivs, iv = getPokeData()
      if seed != -1:
        calc = framecalc(seed)
        starFrame = calc.getStarShinyFrame()
        squareFrame = calc.getSquareShinyFrame()
        starFrameMessage = ""
        squareFrameMessage = ""
        if starFrame != -1:
          starFrameMessage = (starFrame + 1)
        else:
          starFrameMessage = "Frame is greater than 10000! Try again :("
        if squareFrame != -1:
          squareFrameMessage = (starFrame + 1)
        else:
          squareFrameMessage = "Frame is greater than 10000! Try again :("
        embed=discord.Embed(description=f"Here is your information!\n```py\nEncryption Constant: {str(hex(ec))}\nPID: {str(hex(pid))}\nSeed: {seed}\nAmount of IVs: {ivs}\nIVs: {str(iv[0])} / {str(iv[1])} / {str(iv[2])} / {str(iv[3])} / {str(iv[4])} / {str(iv[5])}\nStar Shiny Frames: {starFrameMessage}\nSquare Shiny Frames: {sqaureFrameMessage}```", colour=discord.Colour(value=0xFF00FF))
        await self.userChannel.send(f"<@{self.person.id}>", embed=embed)
        self.userChannel = None
        self.person = None
      else:
        await self.userChannel.send("Invalid seed. Please try a different Pokemon.")
        await self.bot.db.execute(f"DELETE FROM queue WHERE user_id={self.person.id}")
        self.userChannel = None
        self.person = None
 
  @commands.command(name="GetSeed")
  async def obtainSeed(self, ctx, enc=None, pidd=None, ivv=None):
    try:
      ec = int(enc,16)
      pid = int(pidd,16)
      ivs = [ int(iv) for iv in ivv.split("/")]
      gen = seedgen()
      seed, ivs = gen.search(ec, pid, ivs)
      calc = framecalc(seed)
      starFrame = calc.getStarShinyFrame()
      squareFrame = calc.getSquareShinyFrame()
      starFrameMessage = ""
      if starFrame != -1:
        starFrameMessage = str(starFrame + 1)
      else:
        starFrameMessage = "Shiny frame greater than 10000! Try again :("
      squareFrameMessage = ""
      if squareFrame != -1:
        squareFrameMessage = str(squareFrame + 1)
      else:
        squareFrameMessage = "Shiny frame greater than 10000! Try again :("
      await ctx.send("```python\nRaid seed: " + str(seed) + "\nAmount of IVs: " + str(ivs) + "\nStar Shiny at Frame: " + starFrameMessage + "\nSquare Shiny at Frame: " + squareFrameMessage + "```")
    except:
      await ctx.send("Please format your input as: ```$GetSeed [Encryption Constant] [PID] [IVs as HP/Atk/Def/SpA/SpD/Spe]```")


def setup(bot):
  bot.add_cog(RaidCommands(bot))
