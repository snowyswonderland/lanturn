import discord
from discord.ext import commands

class Admin(commands.Cog):
  def __init__(self, bot):
    self.bot = bot

  @commands.command(name="logout")
  async def logout(self, ctx):
    if ctx.author.id in [447548069030920202]:
      await ctx.send("```Shutting down...```")
      await self.bot.logout()
    else:
      await ctx.send("Nice try jackass!")

  @commands.command(name="greet")
  async def greet(self, ctx):
    await ctx.send("Hello everyone! I am Lanturn and I am here to assist you :)")

def setup(bot):
  bot.add_cog(Admin(bot))
