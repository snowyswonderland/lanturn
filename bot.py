import discord, asyncio, sys, os, asyncpg
from discord.ext import commands

class Lanturn(commands.Bot):
  def __init__(self):
    super().__init__(command_prefix='$')
    self.load = [
      'modules.queue',
      'modules.adminstuff'
    ]
    self.path = os.path.dirname(os.path.realpath(__file__))
  
  async def load_db(self):
    #INSTALL PSQL AND ASYNCPG, THIS IS IMPORTANT. ALSO CREATE A USER AND A DATABASE NAMED lanturn AND A TABLE NAMED queue. THIS IS ALSO IMPORTANT
    self.db = await asyncpg.create_pool(host='localhost', user='lanturn', password='password', database='lanturn', min_size=0)

  async def on_ready(self):
    try:
      await self.load_db()
      print('DB LOADED')
    except:
      print('IM STARVING FEED ME YOUR INFO')
    
  def run(self):
    loaded=0
    for ext in self.load:
      try:
        self.load_extension(ext)
        loaded+=1
      except Exception as er:
        print(er)
      print(f"Loaded {loaded} things")

    super().run('TOKEN HERE', reconnect=True)

if __name__ == "__main__":
  Lanturn().run()
